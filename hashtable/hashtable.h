#ifndef HASHTABLE_H
#define HASHTABLE_H

#include <stddef.h>
#include <stdint.h>

#define FNV1A_OFFSET 14695981039346656037UL
#define FNV1A_PRIME 1099511628211UL

#define INITIAL_CAPACITY 16
#define LOAD_FACTOR 0.75f

uint64_t fnv_1a_hash(void *data, size_t length);

/**
 * Every entry in the has table is stored as a key - value struct
 */
struct h_entry {
	void *key;
	void *value;
};

/**
 * Implementing the hashtable structure
 */
struct h_table {
	size_t size;
	size_t capacity;
	size_t key_size;
	size_t value_size;
	// entries of the hash table
	struct h_entry *entries;
	float load_factor;
	uint64_t (*hash)(void *data, size_t length);
};

/**
 * API
 */
struct h_table h_table_init(size_t key_size, size_t value_size);
void h_table_destroy(struct h_table *table);

void h_table_set(struct h_table *table, void *key, void *value);
struct h_entry *h_table_get(struct h_table *table, void *key);
void h_table_delete(struct h_table *table, void *key);

void h_table_resize(struct h_table *table, size_t new_capacity);

struct h_entry h_table_create_entry(void *key, size_t key_length, void *value, size_t value_length);

// Get at most number entries from the table
struct h_entry *h_table_get_entries(struct h_table *table, struct h_entry *dest, size_t number);

int h_compare_keys(void *key1, void *key2, size_t key_length);

size_t h_table_get_index(struct h_table *table, void *key);

#endif

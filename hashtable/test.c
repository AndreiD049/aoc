#include <stddef.h>
#include <stdio.h>
#include "hashtable.h"
#include <assert.h>
#include <string.h>

void test_hash() {
	int a = 4;
	uint64_t hash = fnv_1a_hash(&a, sizeof(int));
	printf("%lu\n", hash);

	char *string = "Hello world";
	uint64_t hash2 = fnv_1a_hash(string, 11);
	printf("%lu\n", hash2);

	printf("Test hash passed\n");
}

void test_init() {
	struct h_table table = h_table_init(0, 0);
	assert(table.size == 0);
	assert(table.capacity == INITIAL_CAPACITY);
	assert(table.load_factor == 0.75f);
	assert(table.entries != NULL);

	h_table_destroy(&table);
	printf("Test init passed\n");
}

void test_set() {
	struct h_table table = h_table_init(sizeof(int), 0);
	int a = 4;
	h_table_set(&table, &a, "Hello world");

	assert(table.size == 1);
	assert(table.capacity == INITIAL_CAPACITY);
	assert(table.entries != NULL);

	h_table_destroy(&table);
	printf("Test set passed\n");
}

void test_resize() {
	struct h_table table = h_table_init(sizeof(int), 0);
	int a = 4;
	h_table_set(&table, &a, "Hello world");

	assert(table.size == 1);
	assert(table.capacity == INITIAL_CAPACITY);
	assert(table.entries != NULL);

	h_table_resize(&table, 2 * INITIAL_CAPACITY);

	assert(table.size == 1);
	assert(table.capacity == 2 * INITIAL_CAPACITY);
	assert(table.entries != NULL);

	h_table_destroy(&table);
	printf("Test resize passed\n");
}

void test_auto_resize() {
	struct h_table table = h_table_init(sizeof(int), 0);
	for (size_t i = 0; i < 100; ++i) {
		h_table_set(&table, &i, "Hello world");
	}

	assert(table.size == 100);
	assert(table.capacity == 256);
	assert(table.entries != NULL);

	h_table_destroy(&table);
	printf("Test auto resize passed\n");
}

void test_get() {
	struct h_table table = h_table_init(0, 0);
	char *key = "Hello world";
	char *value = "Hello world";

	char *key2 = "Hello world2";
	char *value2 = "Hello world2";

	h_table_set(&table, key, value);
	h_table_set(&table, key2, value2);

	struct h_entry *entry = h_table_get(&table, "Hello world");
	assert(entry != NULL);
	assert(strcmp(entry->value, "Hello world") == 0);
	
	struct h_entry *entry2 = h_table_get(&table, "Hello world2");
	assert(entry2 != NULL);
	assert(strcmp(entry2->value, "Hello world2") == 0);
	
	struct h_entry *entry3 = h_table_get(&table, "Hello world3");
	assert(entry3 == NULL);

	h_table_destroy(&table);
	printf("Test get passed\n");
}

void test_delete() {
	// init a hash table
	// set about 100 entries
	// delete 50 entries
	// check that the entries are deleted
	// check that the other entries are still there
	struct h_table table = h_table_init(sizeof(int), 0);
	
	for (size_t i = 0; i < 100; ++i) {
		h_table_set(&table, &i, "Hello world");
	}

	for (size_t i = 0; i < 50; ++i) {
		h_table_delete(&table, &i);
	}

	for (size_t i = 0; i < 50; ++i) {
		struct h_entry *entry = h_table_get(&table, &i);
		assert(entry == NULL);
	}

	for (size_t i = 50; i < 100; ++i) {
		struct h_entry *entry = h_table_get(&table, &i);
		assert(entry != NULL);
	}

	h_table_destroy(&table);
	printf("Test delete passed\n");
}

void test_delete_simple() {
	struct h_table table = h_table_init(sizeof(int), 0);
	int a = 4;
	h_table_set(&table, &a, "Hello world");
	h_table_delete(&table, &a);
	struct h_entry *entry = h_table_get(&table, &a);
	assert(entry == NULL);

	h_table_destroy(&table);
	printf("Test delete simple passed\n");
}

void test_get_entries() {
	struct h_table table = h_table_init(0, 0);
	
	h_table_set(&table, "Hello world", "Hello world");
	h_table_set(&table, "Hello world2", "Hello world2");
	h_table_set(&table, "Hello world3", "Hello world3");

	struct h_entry entries[5];
	h_table_get_entries(&table, entries, sizeof(entries) / sizeof(struct h_entry));

	assert(strcmp(entries[0].key, "Hello world") == 0);
	assert(strcmp(entries[0].value, "Hello world") == 0);
	assert(strcmp(entries[1].key, "Hello world3") == 0);
	assert(strcmp(entries[1].value, "Hello world3") == 0);
	assert(strcmp(entries[2].key, "Hello world2") == 0);
	assert(strcmp(entries[2].value, "Hello world2") == 0);
	assert(entries[3].key == NULL);
	assert(entries[4].key == NULL);

	h_table_destroy(&table);
	printf("Test get entries passed\n");
}

void test_override_val() {
	struct h_table table = h_table_init(sizeof(int), sizeof(int));

	int a = 4;
	int c = 5;
	int b = 1;
	h_table_set(&table, &a, &b);

	b = 2;
	h_table_set(&table, &c, &b);

	struct h_entry *entry = h_table_get(&table, &a);
	assert(entry != NULL);
	assert(*(int *)entry->value == 1);

	struct h_entry *entry2 = h_table_get(&table, &c);
	assert(entry2 != NULL);
	assert(*(int *)entry2->value == 2);

	h_table_destroy(&table);
	printf("Test override val passed\n");
}

int main(void) {
	test_hash();

	test_init();

	test_set();

	test_resize();

	test_auto_resize();

	test_get();

	test_delete_simple();
	test_delete();
	test_get_entries();

	test_override_val();
	return 0;
}

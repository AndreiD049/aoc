#include <stddef.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "hashtable.h"


/*
 * Hash the value stored at pointer *data.
 * The function will read length bytes and will compute the hash
 * based on FNV-1a algorithm - https://en.wikipedia.org/wiki/Fowler%E2%80%93Noll%E2%80%93Vo_hash_function#FNV-1a_hash
 */
uint64_t fnv_1a_hash(void *data, size_t length) {
	uint64_t hash = FNV1A_OFFSET;

	unsigned char *bytes = (unsigned char *)data;
	if (length == 0) {
		// compute string hash
		length = strlen((char *)data);
	}
	for(size_t i = 0; i < length; ++i) {
		hash ^= bytes[i];
		hash *= FNV1A_PRIME;
	}

	return hash;
}

/**
* API
*/
struct h_table h_table_init(size_t key_size, size_t value_size) {
	struct h_table result;
	result.size = 0;
	result.capacity = INITIAL_CAPACITY;
	result.key_size = key_size;
	result.value_size = value_size;
	result.hash = fnv_1a_hash;
	result.entries = calloc(result.capacity, sizeof(struct h_entry));
	result.load_factor = LOAD_FACTOR;

	return result;
}

// Set the value for the given key
void h_table_set(struct h_table *table, void *key, void *value) {
	// Check if table needs to be resized
	if(table->size >= table->capacity * LOAD_FACTOR) {
		h_table_resize(table, table->capacity * 2);
	}
	// allocated an entry and set the data
	struct h_entry entry = h_table_create_entry(key, table->key_size, value, table->value_size);

	size_t index = h_table_get_index(table, key);

	table->entries[index] = entry;
	table->size++;
}

void h_table_delete(struct h_table *table, void *key) {
	size_t index = h_table_get_index(table, key);

	if (table->entries[index].key != NULL) {
		// Free the key
		free(table->entries[index].key);
		table->entries[index].key = NULL;

		// Free the key
		free(table->entries[index].value);
		table->entries[index].value = NULL;

		// Decrease the size
		table->size--;
	}
}

size_t h_table_get_index(struct h_table *table, void *key) {
	uint64_t hash = table->hash(key, table->key_size);
	size_t index = hash % table->capacity;

	while(table->entries[index].key != NULL) {
		if(h_compare_keys(table->entries[index].key, key, table->key_size) == 0) {
			return index;
		}

		index = (index + 1) % table->capacity;
	}

	return index;
}

struct h_entry *h_table_get(struct h_table *table, void *key) {
	size_t index = h_table_get_index(table, key);

	if (table->entries[index].key != NULL) {
		return &table->entries[index];
	}

	return NULL;
}

void h_table_destroy(struct h_table *table) {
	for (size_t i = 0; i < table->capacity; ++i) {
		// The key is freed
		if (table->entries[i].key != NULL) {
			free(table->entries[i].key);
			free(table->entries[i].value);
		}
	}

	free(table->entries);
}

void h_table_resize(struct h_table *table, size_t new_capacity) {
	// Allocate a new table
	struct h_table new_table;
	new_table.size = 0;
	new_table.capacity = new_capacity;
	new_table.key_size = table->key_size;
	new_table.value_size = table->value_size;
	new_table.hash = table->hash;
	new_table.entries = calloc(new_table.capacity, sizeof(struct h_entry));
	new_table.load_factor = table->load_factor;

	// Copy the entries from the old table to the new one
	for(size_t i = 0; i < table->capacity; ++i) {
		if(table->entries[i].key != NULL) {
			h_table_set(&new_table, table->entries[i].key, table->entries[i].value);
		}
	}

	// Free the old table
	h_table_destroy(table);

	// Copy the new table to the old one
	*table = new_table;
}

// Attention: this function allocates memory
struct h_entry h_table_create_entry(void *key, size_t key_length, void *value, size_t value_length) {
	struct h_entry entry;
	
	if (key_length == 0) {
		// compute string length
		key_length = strlen((char *)key) + 1;
	}
	// Copy the key to the entry
	entry.key = malloc(key_length);
	memcpy(entry.key, key, key_length);

	// Value is not copied, it's up to the user to keep it alive
	if (value_length == 0) {
		// compute string length
		value_length = strlen((char *)value) + 1;
	}
	entry.value = malloc(value_length);
	memcpy(entry.value, value, value_length);

	return entry;
}

int h_compare_keys(void *key1, void *key2, size_t key_length) {
	if (key_length == 0) {
		// compare as strings
		return strcmp(key1, key2);
	}
	return memcmp(key1, key2, key_length);
}

struct h_entry *h_table_get_entries(struct h_table *table, struct h_entry *dest, size_t number) {
	size_t count = 0;
	// zero the memory first
	memset(dest, 0, number * sizeof(struct h_entry));

	for (size_t i = 0; i < table->capacity; ++i) {
		if (table->entries[i].key != NULL) {
			dest[count] = table->entries[i];
			count++;
		}
		if (count == number) {
			return dest;
		}
	}

	return dest;
}

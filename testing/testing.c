#include <stddef.h>
#include <stdio.h>
#include <string.h>

static char suites[512][1024];
static size_t suite_idx = 0;

void suite(const char *name) {
	strncpy(suites[suite_idx], name, 1024);
	suite_idx++;
}

void print_suites() {
	for (size_t i = 0; i < suite_idx; i++) {
		printf("Suite %zu: %s\n", i + 1, suites[i]);
	}
}

int main(void) {
	suite("Testing the feature 1");
	suite("Testing the feature 2");
	print_suites();
}

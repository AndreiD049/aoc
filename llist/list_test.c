#include <assert.h>
#include <stdio.h>
#include "llist.h"

int main(void) {
	printf("\t* Testing list init");
	struct l_list list = list_init();
	assert(list.head == NULL);
	assert(list.tail == NULL);
	assert(list.length == 0);
	printf(" - 👌\n");

	printf("\t* Testing appending a value");
	int val = 567;
	list_append(&list, &val);
	assert(list.head != NULL);
	assert(list.tail != NULL);
	assert(list.length == 1);
	assert(*(int *)(list.head->value) == val);
	assert(*(int *)(list.tail->value) == val);
	printf(" - 👌\n");

	printf("\t* Testing appending another value");
	int val2 = 765;
	list_append(&list, &val2);
	assert(list.length == 2);
	assert(*(int *)(list.head->value) == 567);
	assert(*(int *)(list.head->next->value) == 765);
	assert(*(int *)(list.tail->value) == 765);
	printf(" - 👌\n");

	printf("\t* Testing getting an element of a list");
	assert(*(int *)(list_get_el(&list, 0)->value) == 567);
	assert(*(int *)(list_get_el(&list, 1)->value) == 765);
	printf(" - 👌\n");

	printf("\t* Testing poping an element from the tail");
	void *last = list_pop(&list);
	assert(list.length == 1);
	assert(*(int *)last == 765);
	last = list_pop(&list);
	assert(list.length == 0);
	assert(*(int *)last == 567);
	printf(" - 👌\n");

	printf("\t======================\n\tAll tests passed\n");
}

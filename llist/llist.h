/**
 * This is a very fast and dirty linked list implementation
 * It's certainly slow, but i don't care
 */

#ifndef LLIST_H
#define LLIST_H

#include <stddef.h>
 
struct l_node {
	void *value;
	struct l_node *next;
};

struct l_list {
	struct l_node *head;
	struct l_node *tail;
	size_t length;
};

struct l_list list_init();
struct l_node *list_get_el(struct l_list *list, size_t idx);

// !Allocates memory for a new node in the list
// Appends the list to the tail
struct l_node *list_append(struct l_list *list, void *value);

// Removes the element from the list's tail
// !Frees the allocated l_node
// Returns the removed node's value
void *list_pop(struct l_list *list);

// Finds a value in the list based on the value
// Comaprison is done with memcmp
struct l_node *list_find(struct l_list *list, void *value, unsigned long size_bytes);

#endif

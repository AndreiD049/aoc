#include "llist.h"
#include <assert.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

struct l_list list_init() {
	struct l_list result = {
		.head = NULL,
		.tail = NULL,
		.length = 0,
	};
	return result;
}


struct l_node *list_append(struct l_list *list, void *value) {
	// Allocate memory for the l_node
	struct l_node *node = malloc(sizeof(struct l_node));

	node->value = value;
	node->next = NULL;

	// add the node to the list
	if (list->head == NULL) {
		list->head = node;
	}

	if (list->tail != NULL) {
		list->tail->next = node;
	}
	list->tail = node;

	list->length++;

	return node;
}

struct l_node *list_get_el(struct l_list *list, size_t idx) {
	assert(idx < list->length);
	assert(idx >= 0);

	struct l_node *cursor = list->head;
	while (idx--) {
		cursor = cursor->next;
	}
	return cursor;
}

void *list_pop(struct l_list *list) {
	if (list->tail == NULL) {
		return NULL;
	}
	struct l_node *removed_node = list->tail;

	// List has a single element
	if (list->length == 1) {
		list->tail = NULL;
		list->head = NULL;
	} else {
		struct l_node *new_last_node = list_get_el(list, list->length - 2);
		list->tail = new_last_node;
		new_last_node->next = NULL;
	}

	void *result = removed_node->value;
	
	// Free the allocated l_node
	free(removed_node);
	list->length--;
	return result;
}

struct l_node *list_find(struct l_list *list, void *value, unsigned long size_bytes) {
	struct l_node *cursor = list->head;
	while (cursor != NULL) {
		if (memcmp(value, cursor->value, size_bytes) == 0) {
			// Match found
			return cursor;
		}
		cursor = cursor->next;
	}
	return cursor;
}

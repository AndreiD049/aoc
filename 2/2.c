#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/select.h>
#include "../shared.h"

struct gift_box {
	unsigned int l;
	unsigned int w;
	unsigned int h;
};

struct gift_box read_box_dimensions(char *text) {
	struct gift_box result = { 0, 0, 0 };

	// make a buffer for our string and copy the text there
	char t[512];
	strcpy(t, text);

	char *token;
	// read the length
	token = strtok(t, "x");
	result.l = atoi(token);

	// read the width
	token = strtok(NULL, "x");
	aoc_assert(token != NULL, "The string is supposed to be in format [height]x[widthx]x[length]");
	result.w = atoi(token);
	
	// read the height
	token = strtok(NULL, "x");
	aoc_assert(token != NULL, "The string is supposed to be in format [height]x[widthx]x[length]");
	result.h = atoi(token);
	return result;
}

unsigned int get_required_slack(struct gift_box *box) {
	unsigned int wh = box->h * box->w;
	unsigned int min_area = wh;

	unsigned int wl = box->w * box->l;
	if (wl < min_area)
		min_area = wl;

	unsigned int hl = box->h * box->l;
	if (hl < min_area)
		min_area = hl;
	
	return (2 * wh) + (2 * wl) + (2 * hl) + min_area;
}

// Get the total amount of slack required for the whole input file
unsigned int get_total_slack_required(struct sfile_lines *input) {
	unsigned int total = 0;
	for(size_t i = 0; i < input->lines; i++) {
		struct gift_box box = read_box_dimensions(get_line(input, i));
		total += get_required_slack(&box);
	}
	return total;
}

// Get ribbon length required to pack one gift
unsigned int get_ribbon_length(struct gift_box *box) {
	unsigned int perimeters[3] = {  
		box->l + box->w,
		box->l + box->h,
		box->w + box->h
	};
	unsigned int *min_perimeter = min(
		cmp_uint, 
		3,
		&perimeters[0],
		&perimeters[1],
		&perimeters[2]
	);
	return *min_perimeter * 2 + (box->h * box->w * box->l);
}

// Get the length of the ribbon required to pack
// all the gift boxes
unsigned int get_total_ribbon_length(struct sfile_lines *input) {
	unsigned int total = 0;
	for(size_t i = 0; i < input->lines; i++) {
		struct gift_box box = read_box_dimensions(get_line(input, i));
		total += get_ribbon_length(&box);
	}
	return total;
}

int main(int argc, char *argv[]) {
	struct sfile_lines s = read_lines("input.txt");

	// test read_box_dimensions
	struct gift_box gb = read_box_dimensions("10x20x30");
	aoc_assert(gb.l == 10 && gb.w == 20 && gb.h == 30, "Height should be 10, width - 20, length - 30");

	// test get_required_slack
	struct gift_box test_box1 = { 2, 3, 4 };
	aoc_assert(get_required_slack(&test_box1) == 58, "Wrong slack amount");
	struct gift_box test_box2 = { 1, 1, 10 };
	aoc_assert(get_required_slack(&test_box2) == 43, "Wrong slack amount");

	unsigned int result_1 = get_total_slack_required(&s);
	printf("Total slack required - %d\n", result_1);

	// test get_ribbon_length
	aoc_assert(get_ribbon_length(&test_box1) == 34, "Ribbon length expected to be 34");
	aoc_assert(get_ribbon_length(&test_box2) == 14, "Ribbon length expected to be 14");

	unsigned int result_2 = get_total_ribbon_length(&s);
	printf("Total ribbon length - %d\n", result_2);

	// Free the read lines
	free_sfile_lines(&s);
	return 0;
}

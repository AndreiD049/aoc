#ifndef SHARED_H
#define SHARED_H

#include <stddef.h>
#include <stdlib.h>

#define aoc_assert(condition, message) \
    do { \
        if (!(condition)) { \
            fprintf(stderr, "Error: %s\n%s = false\n", message, #condition); \
            exit(EXIT_FAILURE); \
        } \
    } while (0)

/**
* UTILS
*/
typedef void *(*f_comparer)(void *a, void *b);

void *cmp_int(void *a, void *b);
void *cmp_uint(void *a, void *b);

void *min(f_comparer func, size_t count, void *a, ...);

struct sfile_lines {
	size_t lines;
	char **text_lines;
};

// Reads lines from a file
struct sfile_lines read_lines(const char *filename);

// Frees all the lines read from a file
void free_sfile_lines(struct sfile_lines *s);

// Get a line by index
char *get_line(struct sfile_lines *s, size_t idx);

#endif

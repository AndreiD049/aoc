#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <openssl/md5.h>
#include <string.h>
#include <stdbool.h>

bool check_md5(unsigned char *hash) {
	return hash[0] == 0x00 && hash[1] == 0x00 && hash[2] == 0x00;
}

void print_md5(unsigned char *hash) {
	for(size_t i = 0; i < MD5_DIGEST_LENGTH; i++) {
      printf("%02x", hash[i]);
    }
    printf("\n");
}

// To be compiled with: gcc md5.c -o md5 -lssl -lcrypto
int main(int argc, char* argv[]) {
    char* string = argv[1];
    unsigned char hash[MD5_DIGEST_LENGTH];
	unsigned long result = 0;
	char concatenated_hash[100];
	while(result < 100000000) {
		sprintf(concatenated_hash, "%s%lu", string, result);
		MD5((unsigned char*)concatenated_hash, strlen(concatenated_hash), hash);
		if (check_md5(hash)) {
			printf("Found: %s %lu\n", string, result);
			return 0;
		}
		result++;
	}
    return 0;
}

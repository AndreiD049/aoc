#include "../shared.h"
#include <stdio.h>

// Part 1
int get_level(char *line) {
	int level = 0;
	size_t c_idx = 0;
	while (line[c_idx] != '\0') {
		char c = line[c_idx];
		if (c == '(') {
			level++;
		} else if (c == ')') {
			level--;
		}
		c_idx++;
	}
	return level;
}

int get_first_basement_step(const char *line) {
	int step = 0;
	int level = 0;
	int c_idx = 0;
	while (line[c_idx] != '\0') {
		if (line[c_idx] == '(') {
			level++;
		} else if (line[c_idx] == ')') {
			level--;
		}

		// advance step
		step++;
		c_idx++;

		if (level < 0) {
			return step;
		}
	}
	return -1;
}

int main(int argc, char *argv[]) {
	// asserts
	aoc_assert(get_level("(())") == 0, "Unexpected level");
	aoc_assert(get_level("()()") == 0, "Unexpected level");
	aoc_assert(get_level("(((") == 3, "Unexpected level");
	aoc_assert(get_level("))(((((") == 3, "Unexpected level");
	aoc_assert(get_level("))(") == -1, "Unexpected level");
	aoc_assert(get_level(")())())") == -3, "Unexpected level");

	// assert 2nd part
	aoc_assert(get_first_basement_step(")") == 1, "Unexpected first step");
	aoc_assert(get_first_basement_step("()())") == 5, "Unexpected first step");

	struct sfile_lines s = read_lines(argv[1]);	
	int level = get_level(s.text_lines[0]);
	printf("%d\n", level);

	int first_step = get_first_basement_step(s.text_lines[0]);
	printf("First step: %d\n", first_step);

	free_sfile_lines(&s);
	return 0;
}

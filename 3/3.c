#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include "../shared.h"
#include "../hashtable/hashtable.h"

struct location {
	long int line;
	long int column;
};

struct h_table calculate_directions(char *directions) {
	struct location loc = {0, 0};
	struct h_table visited = h_table_init(sizeof(struct location), sizeof(int));

	char *c = directions;
	while (*c != '\0') {
		struct h_entry *l = h_table_get(&visited, &loc);
		if (l == NULL) {
			h_table_set(&visited, &loc, &(int){1});
		} else {
			int *count = l->value;
			(*count)++;
		}
		switch (*c) {
			case '^':
				loc.line++;
				break;
			case 'v':
				loc.line--;
				break;
			case '>':
				loc.column++;
				break;
			case '<':
				loc.column--;
				break;
		}
		c++;
	}

	return visited;
}

struct h_table calculate_directions2(char *directions) {
	struct location santa = {0, 0};
	struct location santa2 = {0, 0};

	struct h_table visited = h_table_init(sizeof(struct location), sizeof(int));
	size_t len = strlen(directions);

	h_table_set(&visited, &santa2, &(int){1});

	for (size_t i = 0; i < len; ++i) {
		struct location *loc = i % 2 == 0 ? &santa : &santa2;
		struct h_entry *l = h_table_get(&visited, loc);
		if (l == NULL) {
			h_table_set(&visited, loc, &(int){1});
		} else {
			int *count = l->value;
			(*count)++;
		}
		switch (directions[i]) {
			case '^':
				loc->line++;
				break;
			case 'v':
				loc->line--;
				break;
			case '>':
				loc->column++;
				break;
			case '<':
				loc->column--;
				break;
		}
	}

	return visited;
}

int main(int argc, char *argv[]) {
	struct sfile_lines s = read_lines(argv[1]);
	char *line = s.text_lines[0];
	printf("%zu line(s) read\n", s.lines);

	struct h_table visited = calculate_directions(line);
	printf("%zu houses visited\n", visited.size);
	h_table_destroy(&visited);

	struct h_table visited2 = calculate_directions2(line);
	printf("%zu houses visited by santa and robot santa\n", visited2.size);

	struct location test = { 0, 0 };
	struct h_entry *e = h_table_get(&visited2, &test);
	printf("Santa visited %d times\n", e == NULL ? 0 : *(int *)e->value);

	h_table_destroy(&visited2);
	free_sfile_lines(&s);
}

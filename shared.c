#include <string.h>
#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdarg.h>
#include "shared.h"

void *cmp_int(void *a, void *b) {
	return *(int *)a < *(int *)b ? a : b;
}

void *cmp_uint(void *a, void *b) {
	return *(unsigned int *)a < *(unsigned int *)b ? a : b;
}


void *min(f_comparer func, size_t count, void *a, ...) {
	va_list va;

	va_start(va, a);

	void *min = a;

	for(size_t i = 0; i < (count - 1); i++) {
		min = func(min, va_arg(va, void *));
	}

	va_end(va);

	return min;	
}

void check_file_exists(FILE *fd, const char *filename) {
	char error[512];
	sprintf(error, "File %s doesn't exist!", filename);
	aoc_assert(fd != NULL, error);
}

size_t count_lines(FILE *fd) {
	assert(fd != NULL);

	size_t lines = 0;
	char c;
	while ((c = fgetc(fd)) != EOF) {
		if (c == '\n')
			lines++;
	}
	rewind(fd);
	return lines;
}

// Count characters until newline or EOF
size_t count_chars(FILE *fd) {
	assert(fd != NULL);

	size_t count = 0;
	char c;
	while ((c = fgetc(fd)) != '\n' && c != EOF) {
		count++;
	}
	if (c == '\n') {
		count++;
	}
	fseek(fd, -count, SEEK_CUR);
	return count;
}

// Reads a line of text from file
// Allocates memory
char *read_line(FILE *fd) {
	assert(fd != NULL);

	size_t c_chars = count_chars(fd) + 1;
	char *result = malloc(sizeof(unsigned char) * c_chars);
	fgets(result, c_chars, fd);
	return result;
}

struct sfile_lines read_lines(const char *filename) {
	FILE *fd = fopen(filename, "r");

	check_file_exists(fd, filename);

	size_t clines = count_lines(fd);
	struct sfile_lines lines = {
		.lines=clines,
		.text_lines=malloc(sizeof(char *) * clines),
	};
	for(int i = 0; i < clines; i++) {
		lines.text_lines[i] = read_line(fd);
	}
	fclose(fd);
	return lines;
}

// Free the struct sfile_lines
void free_sfile_lines(struct sfile_lines *s) {
	// free the strings first
	for(int i = 0; i < s->lines; i++) {
		free(s->text_lines[i]);
	}
	free(s->text_lines);
}

char *get_line(struct sfile_lines *s, size_t idx) {
	assert(s->lines > idx);
	assert(idx >= 0);

	return s->text_lines[idx];
}

#ifdef TEST
void test_min() {
	int ints[] = {1, 2, -3, 4};
	aoc_assert(*(int *)min(cmp_int, 1, &ints[1]) == 2, "Expected min to be = 1");
	aoc_assert(*(int *)min(cmp_int, 2, &ints[0], &ints[1]) == 1, "Expected min to be = 1");
	aoc_assert(*(int *)min(cmp_int, 4, &ints[0], &ints[1], &ints[2], &ints[3]) == -3, "Expected min to be = 1");
}

int main(void) {
	test_min();

	FILE *fd = fopen("./data/one.txt", "r");
	FILE *fd_empty = fopen("./data/empty.txt", "r");
	FILE *fd_multi = fopen("./data/test.txt", "r");
	assert(count_lines(fd_multi) == 7);	
	assert(count_lines(fd_empty) == 0);	
	assert(count_lines(fd) == 1);	

	// test count lines
	assert(count_chars(fd) == 9);
	assert(count_chars(fd_empty) == 0);
	assert(count_chars(fd_multi) == 22);

	char *x = read_line(fd);
	assert(strcmp(x, "one line\n") == 0);

	struct sfile_lines s = read_lines("./data/test.txt");

	fclose(fd);
	fclose(fd_empty);
	fclose(fd_multi);
	free(x);
}
#endif
